'use strict';

import sinon from 'sinon';
import UserController from '../../../app/user/UserController';
import User from '../../../app/shared/models/User';

describe("UserController", function() {
    describe("getUserStreamsCount", function() {
        it("Should not get user streams and send the response if user is not passed in the request", async function() {
            const res = {
                send: sinon.spy()
            };

            const req = {
                params: sinon.spy()
            };

            UserController.getUserStreams = sinon.spy();

            await UserController.getUserStreamsCount(req, res);

            sinon.assert.notCalled(UserController.getUserStreams);
            sinon.assert.called(res.send);
        });

        it("Should get user streams and send the response if user is passed in the request", async function() {
            const res = {
                send: sinon.spy()
            };

            const req = {
                params: {
                    userId: '1'
                }
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2'
            ]);

            await UserController.getUserStreamsCount(req, res);

            sinon.assert.called(User.getUserStreams);
            sinon.assert.called(res.send);
        });
    });

    describe("unlinkUserStream", function() {
        it("Should not delete user stream and send the response if user is not passed in the request", async function() {
            const res = {
                send: sinon.spy()
            };

            const req = {
                params: {
                    streamId: '1'
                }
            };

            UserController.deleteUserStream = sinon.spy();

            await UserController.unlinkUserStream(req, res);

            sinon.assert.notCalled(UserController.deleteUserStream);
            sinon.assert.called(res.send);
        });

        it("Should not delete user stream and send the response if stream is not passed in the request", async function() {
            const res = {
                send: sinon.spy()
            };

            const req = {
                params: {
                    userId: '1'
                }
            };

            UserController.deleteUserStream = sinon.spy();

            await UserController.unlinkUserStream(req, res);

            sinon.assert.notCalled(UserController.deleteUserStream);
            sinon.assert.called(res.send);
        });

        it("Should delete user stream and send the response", async function() {
            const res = {
                send: sinon.spy()
            };

            const req = {
                params: {
                    streamId: '1',
                    userId: '1'
                }
            };

            User.deleteUserStream = sinon.spy();

            await UserController.unlinkUserStream(req, res);

            sinon.assert.called(User.deleteUserStream);
            sinon.assert.called(res.send);
        });
    });
});