'use strict';

import sinon from 'sinon';
import StreamController from '../../../app/stream/StreamController';
import Stream from '../../../app/shared/models/Stream';
import User from '../../../app/shared/models/User';

describe("StreamController", function() {
    describe("getStream", function() {
        it("Should not get stream object and send the response if user is watching 3 streams", async function() {
            const res = {
                send: sinon.spy()
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2', '3'
            ]);

            Stream.getStreamObject = sinon.spy();

            await StreamController.getStream(sinon.spy(), res);

            sinon.assert.notCalled(Stream.getStreamObject);
            sinon.assert.called(res.send);
        });

        it("Should get stream object if user is watching up to 3 streams", async function() {
            const res = {
                send: sinon.spy()
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2'
            ]);

            Stream.getStreamObject = sinon.spy();

            await StreamController.getStream(sinon.spy(), res);

            sinon.assert.called(Stream.getStreamObject);
        });

        it("Should link stream to the user if user is watching up to 3 streams and send response if save fail", async function() {
            const res = {
                send: sinon.spy()
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2'
            ]);

            Stream.getStreamObject = sinon.stub().returns({
                id: '1'
            });
            User.saveUserStream = sinon.spy();

            await StreamController.getStream(sinon.spy(), res);

            sinon.assert.called(User.saveUserStream);
            sinon.assert.called(res.send);
        });

        it("Should link stream to the user if user is watching up to 3 streams and send response if save succeed", async function() {
            const res = {
                send: sinon.spy()
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2'
            ]);

            Stream.getStreamObject = sinon.stub().returns({
                id: '1'
            });
            User.saveUserStream = sinon.stub().returns(true);

            await StreamController.getStream(sinon.spy(), res);

            sinon.assert.called(User.saveUserStream);
            sinon.assert.called(res.send);
        });

        it("Should not link stream to the user and send the response if the stream is not returned", async function() {
            const res = {
                send: sinon.spy()
            };

            User.getUserStreams = sinon.stub().returns([
                '1', '2'
            ]);

            Stream.getStreamObject = sinon.spy();
            User.saveUserStream = sinon.spy();

            await StreamController.getStream(sinon.spy(), res);

            sinon.assert.notCalled(User.saveUserStream);
            sinon.assert.called(res.send);
        });
    });
});