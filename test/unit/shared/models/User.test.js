'use strict';

import {expect} from 'chai';
import User from '../../../../app/shared/models/User';
import RedisClient from '../../../../app/shared/utils/redis-client';
import sinon from "sinon";

describe("User", function() {
    describe("getUserId", function() {
        it("Should return user id string", async function() {
            const userId = User.getUserId();

            expect(typeof userId).to.be.a('string');
        });
    });

    describe("getUserStreams", function() {
        it("Should not get user streams from Redis and return false when user id is not present", async function() {
            User.getUserId = sinon.spy();

            RedisClient.smembers = sinon.spy()
            const redis = {
                smembers: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const userStreams = await User.getUserStreams();

            expect(userStreams).to.equal(false);
            sinon.assert.notCalled(redis.smembers);
        });

        it("Should get user streams from Redis and return them using user id from Auth", async function() {
            User.getUserId = sinon.stub().returns('1');

            const redis = {
                smembers: sinon.stub().returns('test')
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const userStreams = await User.getUserStreams();

            expect(userStreams).to.equal('test');
            sinon.assert.calledWith(redis.smembers, '1:streams');
        });

        it("Should get user streams from Redis and return them using passed user id if available", async function() {
            User.getUserId = sinon.stub().returns('1');

            const redis = {
                smembers: sinon.stub().returns('test')
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const userStreams = await User.getUserStreams('2');

            expect(userStreams).to.equal('test');
            sinon.assert.calledWith(redis.smembers, '2:streams');
        });
    });

    describe("saveUserStream", function() {
        it("Should not link stream to the user and return false when user id is not present and not passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                sadd: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const result = await User.saveUserStream({id: '2'});

            expect(result).to.equal(false);
            sinon.assert.notCalled(redis.sadd);
        });

        it("Should not link stream to the user and return false when stream is not passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                sadd: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const result = await User.saveUserStream(undefined, '1');

            expect(result).to.equal(false);
            sinon.assert.notCalled(redis.sadd);
        });

        it("Should not link stream to the user and return false when stream id is not present", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                sadd: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const result = await User.saveUserStream({test: 'test'}, '1');

            expect(result).to.equal(false);
            sinon.assert.notCalled(redis.sadd);
        });

        it("Should link stream to the user when stream and userId are passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                sadd: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            await User.saveUserStream({id: '2'}, '1');

            sinon.assert.calledWith(redis.sadd, '1:streams', '2');
        });

        it("Should link stream to the user when stream is passed but user id is fetched from Auth", async function() {
            User.getUserId = sinon.stub().returns('1');

            const redis = {
                sadd: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            await User.saveUserStream({id: '2'});

            sinon.assert.calledWith(redis.sadd, '1:streams', '2');
        });
    });

    describe("deleteUserStream", function() {
        it("Should not unlink stream from the user and return false when user id is not present and not passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                srem: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const result = await User.deleteUserStream('1');

            expect(result).to.equal(false);
            sinon.assert.notCalled(redis.srem);
        });

        it("Should not unlink stream from the user and return false when streamId is not passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                srem: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            const result = await User.deleteUserStream(undefined, '1');

            expect(result).to.equal(false);
            sinon.assert.notCalled(redis.srem);
        });

        it("Should unlink stream from the user when streamId and userId are passed", async function() {
            User.getUserId = sinon.spy();

            const redis = {
                srem: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            await User.deleteUserStream('2', '1');

            sinon.assert.calledWith(redis.srem, '1:streams', '2');
        });

        it("Should unlink stream from the user when streamId is passed but user id is fetched from Auth", async function() {
            User.getUserId = sinon.stub().returns('1');

            const redis = {
                srem: sinon.spy()
            };

            User.getRedisClient = sinon.stub().returns(redis);

            await User.deleteUserStream('2');

            sinon.assert.calledWith(redis.srem, '1:streams', '2');
        });
    });

});