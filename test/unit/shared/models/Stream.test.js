'use strict';

import {expect} from 'chai';
import Stream from '../../../../app/shared/models/Stream';

describe("Stream", function() {
    describe("getStreamObject", function() {
        it("Should return stream object", async function() {
            const stream = Stream.getStreamObject();

            expect(stream).to.be.an('object');
        });
    });

    describe("isUserStreamAllowed", function() {
        it("Should return false if user streams are not passed", async function() {
            const isAllowed = Stream.isUserStreamAllowed();

            expect(isAllowed).to.equal(false);
        });

        it("Should return false if user streams false", async function() {
            const isAllowed = Stream.isUserStreamAllowed(false);

            expect(isAllowed).to.equal(false);
        });

        it("Should return true if user streams are empty", async function() {
            const isAllowed = Stream.isUserStreamAllowed([]);

            expect(isAllowed).to.equal(true);
        });

        it("Should return false if there are 3 user streams", async function() {
            const userStreams = [
                '1', '2', '3'
            ];
            const isAllowed = Stream.isUserStreamAllowed(userStreams);

            expect(isAllowed).to.equal(false);
        });

        it("Should return false if there are more than 3 user streams", async function() {
            const userStreams = [
                '1', '2', '3', '4'
            ];
            const isAllowed = Stream.isUserStreamAllowed(userStreams);

            expect(isAllowed).to.equal(false);
        });

        it("Should return true if there are less than 3 user streams", async function() {
            for (let i = 1; i <= 3; i++) {
                const userStreams = [];

                userStreams.push(i);

                const isAllowed = Stream.isUserStreamAllowed(userStreams);

                expect(isAllowed).to.equal(true);
            }
        });
    });

});