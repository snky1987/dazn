# DAZN

DAZN Technical Test - Michal Malinowski

## Test Description
Build a service in your preferred language that exposes an API which can be
consumed from any client. This service must check how many video streams a
given user is watching and prevent a user watching more than 3 video streams
concurrently.

## Assumptions
1. Auth will be provided by external API (assuming micro front-ends architecture) so that user for test application is 
hardcoded as 'bd6e87e0-bbb4-4a7a-9e50-e108d77bb361'.
2. Client will control the user behaviour (e.g. close tab) and will send relevant API calls
to control number of linked user streams.
3. Unspecified in task description 'getStream' function is required in order to prove that
program disallows to watch more than 3 streams concurrently.
4. API can be consumed by any client that can parse JSON string.

## Dev Introduction
This application has been built using Docker to unify environments.
I decided to use NodeJS with Express framework and complied by Babel from ES6. 
For data store I used Redis to ensure that application can be scalable. 
Database would be an overkill for a simple purpose of this application.

Application directory structure mirrors routes structure instead of standard 
MVC directory structure. 
This approach has been working well with every project I was working on.

## Installation
To build the container run: 'docker-compose build dazn'
It will install all dependencies and copy necessary files.

To start the application run: 'docker-compose up'.

Application will be available in the browser on address: http://0.0.0.0:8080

## Routes
Get new fake stream and return JSON response

    /stream

Return the count of user streams

    /user/:userId/streams-count
    
will return the count of user streams

    /user/:userId/stream/:streamId

## Deployment
I did not include any specific deployment strategy but 'npm run test' 
should be run and succeed before deployment can continue.

Application can be deployed as docker container using separate docker-compose file 
(not included in this repository).

As an standard practice deployment hooks should be created for different branches listed below (branch => environment):

1. development    => none
2. staging        => staging
3. pre-production => pre-production
4. master         => production

There is optional QA step and branch but QA could be replaced by staging environment as well.

## Testing
Tests can be run using 'npm run test' command in either local environment 
or preferably in docker container.

I created only unit tests because I did not seen any use for integration tests 
in this application.

I decided to not use any dependency mocking method and used easy to mock or stub getters instead.

I did not include test coverage checks (can be easily implemented) because I was running
out of time and I wanted to submit my work asap.

## Scaling strategy
Horizontal scaling will be guaranteed by pm2 process manager. 
With the current setup it will run and manage nodeJS requests against different processor threads
to improve performance of the application.

pm2 docs: https://pm2.io/doc/en/runtime/overview/

Application can be auto-scaled on EC2 instance using AWS guidelines.

For production purposes ElastiCache should be used for Redis to ensure high scalability
and reliability but Redis can also be run on EC2 instance if heavy customisation is required.