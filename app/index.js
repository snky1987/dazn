'use strict';

import routes from './routes';
import express from 'express';

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.use('/', routes);


app.listen(PORT, HOST);