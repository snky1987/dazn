'use strict';

// Require modules.
import express from 'express';
import StreamController from './stream/StreamController';
import UserController from './user/UserController';

const router = express.Router();

// Routes
router.get('/stream', StreamController.getStream);
router.get('/user/:userId/streams-count', UserController.getUserStreamsCount);
router.get('/user/:userId/stream/:streamId', UserController.unlinkUserStream);

module.exports = router;