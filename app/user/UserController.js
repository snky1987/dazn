'use strict';

import User from '../shared/models/User';

class UserController {

    constructor(User) {
        this.getUserStreamsCount = this.getUserStreamsCount.bind(this);
        this.unlinkUserStream = this.unlinkUserStream.bind(this);

        this.user = User;
    }

    // Return number of streams linked to the user
    async getUserStreamsCount(req, res) {
        const {userId} = req.params;

        if (!userId) {
            return res.send(JSON.stringify({
                success: false,
                error:   'Invalid user.'
            }));
        }

        const userStreams = await this.user.getUserStreams(userId);

        return res.send(JSON.stringify({
            success: true,
            data:    {
                count: userStreams.length
            }
        }));
    }

    // Unlink stream from the user
    async unlinkUserStream(req, res) {
        const {userId, streamId} = req.params;

        if (!userId) {
            return res.send(JSON.stringify({
                success: false,
                error:   'Invalid user.'
            }));
        }

        if (!streamId) {
            return res.send(JSON.stringify({
                success: false,
                error:   'Invalid stream.'
            }));
        }

        await this.user.deleteUserStream(streamId, userId);

        return res.send(JSON.stringify({
            success: true
        }));
    }
}

export default new UserController(User);