import redis from 'redis';
import {promisify} from 'util';

class RedisClient {

    getClient() {
        const client = redis.createClient(process.env.REDIS_URL);

        return {
            ...client,
            smembers:  promisify(client.smembers).bind(client),
            sadd:      promisify(client.sadd).bind(client),
            srem:      promisify(client.srem).bind(client),
        };
    }
}

export default new RedisClient();