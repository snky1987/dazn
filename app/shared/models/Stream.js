'use strict';

class Stream {

    constructor() {
        this.isUserStreamAllowed = this.isUserStreamAllowed.bind(this);
    }

    getStreamObject() {
        return {
            id: Math.random()
        };
    }

    isUserStreamAllowed(userStreams) {
        if (!userStreams) {
            return false;
        }

        return userStreams.length < 3;
    }
}

export default new Stream();