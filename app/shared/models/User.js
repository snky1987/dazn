'use strict';

import RedisClient from '../utils/redis-client';

class User {

    constructor() {
        this.getUserStreams = this.getUserStreams.bind(this);
        this.saveUserStream = this.saveUserStream.bind(this);
        this.deleteUserStream = this.deleteUserStream.bind(this);

        // Redis client cache
        this.redisClient = undefined;
    }

    getUserId() {
        // Hardcoded user for test purposes normally it would come from Auth class or else.
        return 'bd6e87e0-bbb4-4a7a-9e50-e108d77bb361';
    }

    getRedisClient() {
        if (this.redisClient !== undefined) {
            return this.redisClient;
        }

        this.redisClient = RedisClient.getClient();

        return this.redisClient;
    }

    async getUserStreams(id) {
        const userId = id ? id : this.getUserId();

        if (!userId) {
            return false;
        }

        return await this.getRedisClient().smembers(`${userId}:streams`);
    }

    async saveUserStream(stream, id) {
        const userId = id ? id : this.getUserId();

        if (!userId || !stream || (stream && stream.id === undefined)) {
            return false;
        }

        return await this.getRedisClient().sadd(`${userId}:streams`, stream.id);
    }

    async deleteUserStream(streamId, id) {
        const userId = id ? id : this.getUserId();

        if (!userId || !streamId) {
            return false;
        }

        return await this.getRedisClient().srem(`${userId}:streams`, streamId);
    }
}

export default new User();