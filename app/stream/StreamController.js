'use strict';

import User from '../shared/models/User';
import Stream from '../shared/models/Stream';

class StreamController {

    constructor(Stream, User) {
        this.getStream = this.getStream.bind(this);

        this.stream = Stream;
        this.user = User;
    }

    /*
     * Checks if user can access the stream
     * Links stream id to the user
     * Return stream if user is not already watching more than 3 streams
     */
    async getStream(req, res) {
        const userStreams = await this.user.getUserStreams();
        const isAllowed = Stream.isUserStreamAllowed(userStreams);

        // Check if user can access the stream
        if (isAllowed === false) {
            return res.send(JSON.stringify({
                success: false,
                error:   'User cannot watch more than 3 streams concurrently.'
            }));
        }

        const stream = this.stream.getStreamObject();

        // Check if stream exists
        if (!stream) {
            return res.send(JSON.stringify({
                success: false,
                error:   'Stream error.'
            }));
        }

        const saved = await this.user.saveUserStream(stream);

        // Check if stream has been linked to the user
        if (!saved) {
            return res.send(JSON.stringify({
                success: false,
                error:   'Stream error.'
            }));
        }

        res.send(JSON.stringify({
            success: true,
            stream
        }));
    }
}

export default new StreamController(Stream, User);